const CLI = require('clui');
const Configstore = require('configstore');
const Spinner = CLI.Spinner;
const inquirer = require('./inquirer');
const pkg = require('../package.json');
const conf = new Configstore(pkg.name);
var {Bitbucket} = require("bitbucket");
const fs = require('fs');
const git = require('simple-git/promise')();
const touch = require("touch");
const _ = require('lodash');

module.exports = {
  createRemoteRepo: async () => {
    const credentials = await inquirer.askBitbucketCredentials();
    const answers = await inquirer.askRepoDetails();

    const clientOptions = {
      auth: {
        username: credentials.username,
        password: credentials.password,
      },
    };

    const bitbucket = new Bitbucket(clientOptions);
    const repo_slug = answers.name;
    const workspace = 'miljkovicmisa';
    const _body = { is_private: answers.visibility === 'private', description: answers.description };

    const status = new Spinner('Creating remote repository...');

    status.start();

    try {
      const { data, headers, status, url } = await bitbucket.repositories.create({ _body, repo_slug, workspace });
      return data.links.clone[1].href;
    } catch (err) {
      const { message, error, headers, request, status } = err;
      return error.error.fields.name;
    } finally {
      status.stop();
    }


  },
};
