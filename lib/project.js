const inquirer = require('./inquirer');
const { exec } = require('child_process');

module.exports = {
    createProject: async () => {
        const projectType = await inquirer.askProjectDetails();

        if(projectType.projectType == 'Wordpress'){
            exec("wp core version", (error, stdout, stderr) => {
                if (error) {
                    console.log(`error: ${error.message}`);
                    return;
                }
                if (stderr) {
                    console.log(`stderr: ${stderr}`);
                    return;
                }
                console.log(`stdout: ${stdout}`);
            });
        } else {
            return 'Owww!';
        }
    }
}