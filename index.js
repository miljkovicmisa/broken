#!/usr/bin/env node
const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');
const files = require('./lib/files');
const Configstore = require('configstore');
const conf = new Configstore('broken');
const bitbucket = require('./lib/bitbucket');
const project = require('./lib/project');

clear();

console.log(
  chalk.yellow(
    figlet.textSync('broken', { font: 'Ghost', horizontalLayout: 'full' })
  )
);
console.log(
  chalk.white(
    figlet.textSync('by WHITEHAT', { horizontalLayout: 'default', width: 80 })
  )
);

// if (files.directoryExists('.git')) {
//   console.log(chalk.red('Already a Git repository!'));
//   process.exit();
// }

const run = async () => {
    const projectType = await project.createProject();
    console.log(projectType);
    const url = await bitbucket.createRemoteRepo();
    console.log(url);
  };

run();

